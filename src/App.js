import React, { Component } from 'react';
import './App.css';
import ProgressComponent from './components/ProgressComponent';
import TaskAdderComponent from './components/TaskAdderComponent';
import TaskListComponent from './components/TaskListComponent';
import Task from './model/Task';
const uuidv4 = require('uuid/v4');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: []
    }
  }

  taskCompletionCallback = (task, isCompleted) => {
    const updatedTasks = [...this.state.tasks].map(t => {
      if (t.id !== task.id) {
        return t;
      }
      t.isCompleted = isCompleted;
      return t;
    });
    this.setState({
      tasks: updatedTasks
    });
  }

  addTask = (taskText) => {
    const newTask = new Task();
    newTask.text = taskText;
    newTask.isCompleted = false;
    newTask.id = uuidv4();
    const newTasksList = [...this.state.tasks];
    newTasksList.push(newTask);

    this.setState({
      tasks: newTasksList
    });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col" style={{marginBottom: "2rem"}}>
            <ProgressComponent
              completed={this.state.tasks.filter(t => t.isCompleted).length}
              total={this.state.tasks.length} />
          </div>
        </div>
        <div className="row">
          <div className="col" style={{ marginBottom: "2rem" }}>
            <TaskAdderComponent
              inputTextCallback={(taskText) => {
                this.addTask(taskText);
              }} />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <TaskListComponent
              tasks={this.state.tasks}
              callback={this.taskCompletionCallback} />
          </div>
        </div>
      </div>
    );
  }
}
export default App;
