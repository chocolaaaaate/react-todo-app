import React, { Component } from 'react';

class TaskAdderComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: ""
        }
    }

    render() {
        return (
            <div>
                <input
                    type="text"
                    className="col-10"
                    value={this.state.text}
                    placeholder="Add a task"
                    onChange={(e) => {
                        this.setState({
                            text: e.target.value
                        });
                    }}
                    style={{
                        border: "0px",
                        borderBottom: "1px dashed black"
                    }} />
                <button
                    className="col-2"
                    onClick={() => {
                        this.props.inputTextCallback(this.state.text);
                        this.setState({
                            text: ""
                        })
                    }}
                    style={{
                        background: "black",
                        color: "whitesmoke"
                    }}
                >+</button>
            </div>
        );
    }
}

export default TaskAdderComponent;
