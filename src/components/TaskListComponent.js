import React, { Component } from 'react';
import TaskComponent from './TaskComponent';
import '../css/TaskListComponent.css';

class TaskListComponent extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col">
                        <h3 style={{
                            color: "#007bff",
                            marginBottom: "1rem"
                        }}>Tasks</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <table className="col">
                            {this.props.tasks.map(t =>
                                <tr className="taskListTableRow">
                                    <TaskComponent
                                        task={t}
                                        callback={this.props.callback}
                                        key={t.id}
                                    />
                                </tr>
                            )}
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default TaskListComponent; 
