import React, { Component } from 'react';

class TaskComponent extends Component {

    getFormattedTask = (task) => {
        if (task.isCompleted) {
            return <s style={{
                color: "#007bff"
            }}>{task.text}</s>
        }
        return <span>{task.text}</span>;
    }

    render() {
        return (
            <div>
                <input
                    type="checkbox"
                    checked={this.props.task.isCompleted}
                    onChange={(e) =>
                        this.props.callback(this.props.task, e.target.checked)
                    }
                    style={{
                        marginRight: "1rem"
                    }}
                />
                <span>
                    {this.getFormattedTask(this.props.task)}
                </span>
            </div>
        );
    }
}

export default TaskComponent;
