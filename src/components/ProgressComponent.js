import React, { Component } from 'react';
import { ProgressBar } from 'react-bootstrap';
import '../css/ProgressBar.css'

class ProgressComponent extends Component {
    render() {
        return (
            <span>
                <ProgressBar
                    now={this.props.completed}
                    max={this.props.total}/>
            </span>
        );
    }
}

export default ProgressComponent;
